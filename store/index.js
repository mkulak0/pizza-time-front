export const state = () => ({
    sidebarOpen: false
})

export const mutations = {
    openSidebar(state){
        state.sidebarOpen = true;
    },
    closeSidebar(state){
        state.sidebarOpen = false;
    }
}