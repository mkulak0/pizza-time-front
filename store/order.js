export const state = () => ({
    showNewPizzaModal: false,
    newPizza: {},
    ordered: [],
    orderedAddons: [],
})

export const mutations = {
    showModal(state, payload){
        state.showNewPizzaModal = true;
        state.newPizza = payload;
    },

    setNewPizza(state, payload){
        state.newPizza = payload;
    },

    setShow(state, payload){
        state.showNewPizzaModal = payload;
    },

    addNewPizza(state, payload){
        state.ordered.push(payload);
        state.newPizza = {};
        state.showNewPizzaModal = false;
    },

    addNewAddon(state, payload){
        state.orderedAddons.push(payload);
    }
}