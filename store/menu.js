export const state = () => ({
    predefinedPizzas: [
        {
            name: "pepperoni",
            display: "Pepperoni",
            ingradients: ['cheese', 'pepperoni'],
            color: 'danger'
        },
        {
            name: "hawaii",
            display: "Hawajska",
            ingradients: ['cheese', 'pineapple'],
            color: 'warning'
        },
        {
            name: "margharita",
            display: "Margarita",
            ingradients: ['cheese'],
            color: 'grey-lighter'
        }
    ],
    ingradients: [
        {
            name: "cheese",
            display: "Ser Żółty",
            cost: 3,
        },
        {
            name: "pepperoni",
            display: "Kiełbasa pepperoni",
            cost: 4,
        },
        {
            name: "pineapple",
            display: "Ananas",
            cost: 3
        },
    ],
    sizes:[
        {
            size: 'small',
            display: "Mała",
            cost: 10
        },
        {
            size: 'medium',
            display: "Średnia",
            cost: 20
        },
        {
            size: "big",
            display: "Duża",
            cost: 30
        },
    ],
    drinks: [
        {
            name: 'fanta',
            display: 'Fanta',
            cost: '5',
        },
        {
            name: 'cola',
            display: 'Coca-cola',
            cost: '6',
        }
    ],
    sauce: [
        {
            name: 'tomato',
            display: 'Sos pomidorowy',
            cost: '5',
        },
        {
            name: 'garlic',
            display: 'Sos czosnkowy',
            cost: '5',
        },
        {
            name: 'special',
            display: 'Specjalny sos dnia',
            cost: '7',
        }
    ],
    dishes: [
        {
            name: 'fries-small',
            display: 'Małe frytki',
            cost: '4',
        },
        {
            name: 'fries-big',
            display: 'Duże frytki',
            cost: '7',
        }
    ]
})

export const mutations = {
    
}